include ../kubernetes/etc/help.mk
include ../kubernetes/etc/cli.mk

.PHONY: system-requirements-check
system-requirements-check: ##@setup checks system for required dependencies
	./etc/system-requirements-check.sh

.PHONY: deploy
deploy: ##@setup deploy to nodes
	$(CLI) kubectl apply \
		-f nginx-namespace.yaml \
		-f . \
		-f echo-server/namespace.yaml \
		-f echo-server/
	$(CLI) kubectl delete --ignore-not-found=true ns kube-lego
	$(CLI) kubectl delete --ignore-not-found=true -n ingress-nginx daemonset/nginx-ingress-controller

.PHONY: update
update: ##@setup download current definitions
	curl -o nginx-namespace.yaml        https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/namespace.yaml
	curl -o default-backend.yaml        https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/default-backend.yaml
	curl -o nginx-configmap.yaml        https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/configmap.yaml
	curl -o tcp-services-configmap.yaml https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/tcp-services-configmap.yaml
	curl -o udp-services-configmap.yaml https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/udp-services-configmap.yaml
	curl -o nginx-rbac.yaml             https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/rbac.yaml
	curl -o nginx-deployment.yaml       https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/with-rbac.yaml
	curl -o service-nodeport.yaml       https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml
	curl -o cert-manager-with-rbac.yaml https://raw.githubusercontent.com/jetstack/cert-manager/master/contrib/manifests/cert-manager/with-rbac.yaml
