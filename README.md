# Ingress

A deployment of Nginx Ingress https://github.com/kubernetes/ingress-nginx  
Gets Let's Encrypt certificates http://docs.cert-manager.io/en/latest/index.html

## Usage

run `make` for usage

## Access

- HTTP on node port 80
- HTTPS on node port 443
